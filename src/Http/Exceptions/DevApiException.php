<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\Http\Exceptions;

use Thrustbit\DevApi\Http\Exceptions\Contract\ApiError;

class DevApiException extends \RuntimeException implements ApiError
{
}