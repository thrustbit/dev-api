<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\ServiceBus;

use Illuminate\Http\Request;

class NoopMetadata implements MetadataGatherer
{

    public function fromRequest(Request $request): array
    {
        return [];
    }
}