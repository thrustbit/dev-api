<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\Http\Providers;

use Illuminate\Support\ServiceProvider;
use Prooph\Common\Messaging\FQCNMessageFactory;
use Prooph\Common\Messaging\MessageConverter;
use Prooph\Common\Messaging\MessageFactory;
use Prooph\Common\Messaging\NoOpMessageConverter;
use Thrustbit\DevApi\Http\Response\JsonResponse;
use Thrustbit\DevApi\Http\Response\ResponseStrategy;
use Thrustbit\DevApi\ServiceBus\MetadataGatherer;
use Thrustbit\DevApi\ServiceBus\NoopMetadata;

class DevApiServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $services = [
        MetadataGatherer::class => NoopMetadata::class,
        MessageConverter::class => NoOpMessageConverter::class,
        ResponseStrategy::class => JsonResponse::class,
        MessageFactory::class => FQCNMessageFactory::class
    ];

    public function register(): void
    {
        $this->registerServices();
    }

    private function registerServices(): void
    {
        // services must be in configuration
        // as Message finder
        foreach ($this->services as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }

    public function provides(): array
    {
        return array_keys($this->services);
    }
}