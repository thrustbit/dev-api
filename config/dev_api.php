<?php

return [
    'messages' => [

        'command_api' => [
            /* list of command messages class */
        ],

        'query_api' => [
            /* list of query messages class */
        ]
    ]
];