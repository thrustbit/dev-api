<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\ServiceBus;

use Illuminate\Http\Request;

interface MetadataGatherer
{
    public function fromRequest(Request $request): array;
}