<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\Http\Exceptions;

class MissingMessageNameAttribute extends DevApiException
{
    public static function withKeys(string $attributeName, string $busType): MissingMessageNameAttribute
    {
        return new self(
            sprintf('Missing attribute name %s for %s bus', $attributeName, $busType)
        );
    }
}