<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

abstract class MessageAware
{
    /**
     * @var Collection
     */
    private $messages;

    public function __construct(Collection $messages)
    {
        $this->messages = $messages;
    }

    protected function findMessageIn(Request $request): ?string
    {
        $routeName = $this->findRouteName($request);

        if( !$routeName){
            return null;
        }

        return $this->messages->get($routeName);
    }

    protected function findRouteName(Request $request): ?string
    {
        return $request->route()->getName();
    }
}