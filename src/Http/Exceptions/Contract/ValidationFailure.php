<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\Http\Exceptions\Contract;

interface ValidationFailure extends ClientError
{
}