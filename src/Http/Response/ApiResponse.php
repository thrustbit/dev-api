<?php

declare(strict_types=1);

namespace Thrustbit\DevApi\Http\Response;

use Symfony\Component\HttpFoundation\Response;

interface ApiResponse
{
    public function respondTo(\Throwable $exception): Response;
}